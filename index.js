let text = "marvel mola!";
let textWithLine = "";

for (i = 0; i < text.length; i++) {
    let hasSpaceAfter = text.charAt(i + 1) == " "
    let isSpace = text.charAt(i) == " "


    if (!isSpace) {
        textWithLine += text.charAt(i) + (hasSpaceAfter ? "" : "-");
    } else {
        textWithLine += " "
    }
}

let expectedTextResult = textWithLine.substring(0, textWithLine.length - 1);

console.log(expectedTextResult)